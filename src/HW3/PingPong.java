package HW3;

public class PingPong {

    public static void main(String[] args) {

        Object table = new Object();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (table) {
                        try {
                            System.out.println("ping!");
                            table.notify();
                            table.wait();
                            Thread.sleep((int) (Math.random() * 5000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (table) {
                        try {
                            System.out.println("pong!");
                            table.notify();
                            table.wait();
                            Thread.sleep((int) (Math.random() * 5000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
}
