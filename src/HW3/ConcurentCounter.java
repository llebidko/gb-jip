package HW3;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurentCounter {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 10; j++) {
                        counter.increase();
                    }
                }
            }).start();
        }

        Thread.sleep(2000);
        System.out.println(counter.counter);
    }
}

class Counter {
    Integer counter;
    private Lock lock;

    public Counter() {
        counter = new Integer(0);
        lock = new ReentrantLock();
    }

    void increase() {
        while (!lock.tryLock()) {
            System.out.println("locked...");
        }
        counter += 1;
        lock.unlock();
    }
}
