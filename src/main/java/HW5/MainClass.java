package HW5;

import HW5.dao.StudentDao;
import HW5.entity.Student;

public class MainClass {
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDao();
        studentDao.openCurrentSessionwithTransaction();

        // Добавление 1000 студентов и микро-проверки
        for (int i = 0; i < 1000; i++) studentDao.persist(new Student("test" + i, i));
        Student founded = studentDao.findById(189);
        System.out.println(founded.getName());
        studentDao.delete(founded);

        studentDao.closeCurrentSessionwithTransaction();
    }
}
