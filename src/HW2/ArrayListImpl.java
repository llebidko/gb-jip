package HW2;

public class ArrayListImpl<T> {

    private T[] internalArray;
    private int size;
    private int capacity;

    public ArrayListImpl() {
        this.capacity = 10;
        this.size = 0;
        this.internalArray = (T[]) new Object[capacity];
    }

    public boolean add(T element) {
        if (size == capacity) addCapacity();
        internalArray[size] = element;
        size++;
        return true;
    }

    public boolean add(int position, T element) {
        if (size == capacity) addCapacity();
        if (position < 0 || position > size) return false;
        for (int i = size; i > position; i--)
            internalArray[i] = internalArray[i - 1];
        internalArray[position] = element;
        size++;
        return true;
    }

    public int indexOf(T element) {
        int returnedIndex = -1;
        for (int i = 0; i < size; i++)
            if (element == internalArray[i]) {
                returnedIndex = i;
                break;
            }
        return returnedIndex;
    }

    public boolean contains(T element) {
        return (indexOf(element) != -1);
    }


    public boolean remove(T element) {
        int index = indexOf(element);
        if (index == -1) return false;
        for (int i = index; i < size - 1; i++)
            internalArray[i] = internalArray[i + 1];
        size--;
        return true;
    }

    public boolean set(int position, T element) {
        if (position < 0 || position > size - 1) return false;
        internalArray[position] = element;
        return true;
    }

    public boolean clear() {
        size = 0;
        return true;
    }

    public T[] toArray() {
        T[] tempArray = (T[]) new Object[size];
        for (int i = 0; i < size; i++)
            tempArray[i] = internalArray[i];
        return (T[]) tempArray;
    }

    private void addCapacity() {
        T[] tempArray = (T[]) new Object[size];
        for (int i = 0; i < size; i++)
            tempArray[i] = internalArray[i];
        capacity += 10;
        internalArray = tempArray;
    }
}
