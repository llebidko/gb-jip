package HW2;

public class LinkedListImpl<T> {

    private Element firstElement;

    public LinkedListImpl() {
    }

    public boolean addFirst(T value) {
        Element insertedElement = new Element(value);
        insertedElement.nextElement = firstElement;
        firstElement = insertedElement;
        return true;
    }

    public boolean addLast(T value) {
        Element insertedElement = new Element(value);
        if (firstElement == null) {
            firstElement = insertedElement;
            return true;
        }
        Element positionNext = firstElement.nextElement;
        Element positionCurrent = firstElement;
        while (positionNext != null) {
            positionCurrent = positionNext;
            positionNext = positionNext.nextElement;
        }
        positionCurrent.nextElement = insertedElement;
        return true;
    }

    public boolean removeFirst() {
        if (firstElement == null) return false;
        firstElement = firstElement.nextElement;
        return true;
    }

    public boolean removeLast() {
        if (firstElement == null) return false;
        if (firstElement.nextElement == null) {
            firstElement = null;
            return false;
        }
        Element positionNext = firstElement.nextElement;
        Element positionCurrent = firstElement;
        while (positionNext.nextElement != null) {
            positionCurrent = positionNext;
            positionNext = positionNext.nextElement;
        }
        positionCurrent.nextElement = null;
        return true;
    }

    public T getFirst() {
        if (firstElement == null) throw new RuntimeException("Нет элементов");
        return firstElement.value;
    }

    public T getLast() {
        if (firstElement == null) throw new RuntimeException("Нет элементов");
        Element positionNext = firstElement.nextElement;
        Element positionCurrent = firstElement;
        while (positionNext != null) {
            positionCurrent = positionNext;
            positionNext = positionNext.nextElement;
        }
        return positionCurrent.value;
    }

    public String toString() {
        if (firstElement == null) return "[]";
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        Element position = firstElement;
        do {
            sb.append(position.value); // надеемся что есть toString
            sb.append(", ");
            position = position.nextElement;
        } while (position != null);
        sb.delete(sb.length() - 2, sb.length());
        sb.append(']');
        return sb.toString();
    }

    class Element {
        Element nextElement;
        T value;

        Element(T value) {
            this.value = value;
        }
    }
}
