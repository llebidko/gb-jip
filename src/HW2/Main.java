package HW2;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        System.out.println("ArrayListImpl demo:");
        ArrayListImpl<Integer> myArrayList = new ArrayListImpl();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(2, 3);
        System.out.println("Misc add to array: " + Arrays.toString(myArrayList.toArray()));
        System.out.println("Check 6 in array: " + myArrayList.contains(6));
        System.out.println("Check index of 3 in array: " + myArrayList.indexOf(3));
        myArrayList.remove(1);
        System.out.println("Remove 1 from array: " + Arrays.toString(myArrayList.toArray()) + "\n");

        System.out.println("LinkedListImpl demo:");
        LinkedListImpl<Integer> myLinkedList = new LinkedListImpl();
        myLinkedList.addFirst(3);
        myLinkedList.addLast(4);
        myLinkedList.addFirst(2);
        myLinkedList.addLast(5);
        myLinkedList.addFirst(1);
        System.out.println("Misc add to list: " + myLinkedList);
        System.out.println("First element: " + myLinkedList.getFirst());
        System.out.println("Last element: " + myLinkedList.getLast());
        myLinkedList.removeFirst();
        myLinkedList.removeLast();
        System.out.println("Remove first and last from list: " + myLinkedList);
    }
}
