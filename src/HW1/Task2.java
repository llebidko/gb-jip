package HW1;

// Описать ошибки в коде и предложить варианты оптимизации
public class Task2 {

    interface Moveable {
        void move();
    }

    interface Stopable {
        void stop();
    }

    // Добавлен новый класс
    class Engine {
    }

    abstract class Car {
        public Engine engine; // не было класса Engine
        private String color;
        private String name;

        protected void start() {
            System.out.println("Car starting");
        }

        abstract void open();

        public Engine getEngine() {
            return engine;
        }

        public void setEngine(Engine engine) {
            this.engine = engine;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    class LightWeightCar extends Car implements Moveable {

        @Override
        void open() {
            System.out.println("Car is open");
        }

        @Override
        public void move() {
            System.out.println("Car is moving");
        }

    }

    class Lorry extends Car implements Moveable, Stopable { // Было: class Lorry extends Car, Moveable, Stopable

        public void move() {
            System.out.println("Car is moving");
        }

        public void stop() {
            System.out.println("Car is stop");
        }

        //абстрактный метод необходимо реализовать при наследовании от абстрактного класса
        @Override
        void open() {
            System.out.println("Lorry is open");
        }
    }
}