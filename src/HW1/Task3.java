package HW1;

// Написать пример кода, который реализует принцип полиморфизма, на примере фигур — круг, квадрат, треугольник.

public class Task3 {

    abstract class Shape {
        int size;

        abstract int getSize();
    }

    class Circle extends Shape {
        @Override
        int getSize() {
            //some code for circleSize
            return size;
        }
    }

    class Square extends Shape {
        @Override
        int getSize() {
            //some code for squareSize
            return size;
        }
    }

    class Triangle extends Shape {
        @Override
        int getSize() {
            //some code for triangleSize
            return size;
        }
    }
}
