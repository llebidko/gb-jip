package HW1;

/* Создать builder для класса HW1.Person со следующими полями:
String firstName, String lastName, String middleName, String country, String address, String phone, int age, String gender.
*/

public class Task1 {
    public static void main(String[] args) {
        Person person = new Person.Builder()
                .addAddress("Москва")
                .addFirstName("Иван")
                .addLastName("Иванов")
                .addAge(22)
                .build();
        System.out.println(person);
    }
}

class Person {
    private String firstName;
    private String lastName;
    private String middleName;
    private String country;
    private String address;
    private String phone;
    private int age;
    private String gender;

    @Override
    public String toString() {
        return "First Name = " + firstName + "\n" +
                "Middle Name = " + middleName + "\n" +
                "Last Name = " + lastName + "\n" +
                "Country = " + country + "\n" +
                "Address = " + address + "\n" +
                "Phone = " + phone + "\n" +
                "Age = " + age + "\n" +
                "Gender = " + gender;
    }

    public static class Builder {

        private Person person;

        public Builder() {
            this.person = new Person();
        }

        public Builder addFirstName(String firstName) {
            person.firstName = firstName;
            return this;
        }

        public Builder addLastName(String lastName) {
            person.lastName = lastName;
            return this;
        }

        public Builder addMiddleName(String middleName) {
            person.middleName = middleName;
            return this;
        }

        public Builder addCountry(String country) {
            person.country = country;
            return this;
        }

        public Builder addAddress(String address) {
            person.address = address;
            return this;
        }

        public Builder addPhone(String phone) {
            person.phone = phone;
            return this;
        }

        public Builder addAge(int age) {
            person.age = age;
            return this;
        }

        public Builder addGender(String gender) {
            person.gender = gender;
            return this;
        }

        public Person build() {
            return person;
        }
    }
}

