-- "Составить грамотную нормализованную схему хранения этих данных в БД"...
CREATE SCHEMA `cinema` DEFAULT CHARACTER SET utf8mb4;

-- "У фильма, который идет в кинотеатре, есть название, длительность (пусть будет 60, 90 или 120 минут)"...
CREATE TABLE `cinema`.`movie` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(90) NOT NULL,
  `duration` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- "цена билета (в разное время и дни может быть разной), время начала сеанса (один фильм может быть показан
-- несколько раз в разное время и с разной ценой билета)"...
CREATE TABLE `cinema`.`ticket_price` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `movie_id` INT NOT NULL,
  `event_start` DATETIME NOT NULL,
  `price` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `movie_ticket_price_idx` (`movie_id` ASC),
  CONSTRAINT `movie_ticket_price`
    FOREIGN KEY (`movie_id`)
    REFERENCES `cinema`.`movie` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- "Есть информация о купленных билетах (номер билета, на какой сеанс)"...
CREATE TABLE `cinema`.`purchase` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticket_price_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `purchase_ticket_price_idx` (`ticket_price_id` ASC),
  CONSTRAINT `purchase_ticket_price`
    FOREIGN KEY (`ticket_price_id`)
    REFERENCES `cinema`.`ticket_price` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- "Внести в нее 4–5 фильмов"...
INSERT INTO `cinema`.`movie` (`name`, `duration`) VALUES ('Мэри Поппинс возвращается', '02:00:00');
INSERT INTO `cinema`.`movie` (`name`, `duration`) VALUES ('Три богатыря и наследница престола', '01:00:00');
INSERT INTO `cinema`.`movie` (`name`, `duration`) VALUES ('Аквамен', '02:00:00');
INSERT INTO `cinema`.`movie` (`name`, `duration`) VALUES ('Дом, который построил Джек', '01:30:00');

-- "расписание на один день"...
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('1', '2019-01-09 09:00:00', '10');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('1', '2019-01-09 12:00:00', '11');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('1', '2019-01-09 15:00:00', '12');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('1', '2019-01-09 21:00:00', '14');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('1', '2019-01-09 18:00:00', '13');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('2', '2019-01-09 11:00:00', '5');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('2', '2019-01-09 14:00:00', '5');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('2', '2019-01-09 17:00:00', '5');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('3', '2019-01-09 10:00:00', '14');
INSERT INTO `cinema`.`ticket_price` (`movie_id`, `event_start`, `price`) VALUES ('4', '2019-01-09 16:45:00', '15');

-- "и несколько проданных билетов"...
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('1');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('1');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('2');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('2');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('3');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('3');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('4');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('4');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('5');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('6');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('7');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('8');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('9');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('10');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('1');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('2');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('3');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('4');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('5');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('6');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('7');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('8');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('9');
INSERT INTO `cinema`.`purchase` (`ticket_price_id`) VALUES ('10');

-- "ошибки в расписании (фильмы накладываются друг на друга), отсортированные по возрастанию времени.
-- Выводить надо колонки «фильм 1», «время начала», «длительность», «фильм 2», «время начала», «длительность»"...
SELECT
  	t1.name AS фильм_1,
    t1.event_start AS время_начала,
    t1.duration AS длительность,
    t2.name AS фильм_2,
    t2.event_start AS время_начала,
    t2.duration AS длительность
FROM
   	(SELECT
	  	tp.id,
  		m.duration,
  		m.name,
			tp.event_start,
			ADDTIME(tp.event_start, m.duration) AS event_end
		FROM
			movie m
		LEFT JOIN ticket_price tp ON m.id = tp.movie_id)
		AS t1
  CROSS JOIN
	  (SELECT
			tp.id,
			m.duration,
			m.name,
			tp.event_start,
			ADDTIME(tp.event_start, m.duration) AS event_end
		FROM
		  movie m
		LEFT JOIN ticket_price tp ON m.id = tp.movie_id)
		AS t2
WHERE
	t1.id <> t2.id
	AND
		((t1.event_start < t2.event_start AND t1.event_end > t2.event_start)
    OR
    (t1.event_start > t2.event_start AND t1.event_start < t2.event_end)
    OR
    (t1.event_start > t2.event_start AND t1.event_end < t2.event_end))
ORDER BY t1.event_start;

-- "перерывы 30 минут и более между фильмами — выводить по уменьшению длительности перерыва.
-- Колонки «фильм 1», «время начала», «длительность», «время начала второго фильма», «длительность перерыва»;"
SELECT * FROM
	(SELECT
		t1.name AS фильм_1,
		t1.event_start AS время_начала,
		t1.duration AS длительность,
		t2.event_start AS следующий_фильм_в,
		MIN(TIMEDIFF(t2.event_start, t1.event_end)) AS длительность_перерыва
	FROM
			(SELECT
				tp.id,
				m.name,
        m.duration,
				tp.event_start,
				ADDTIME(tp.event_start, m.duration) AS event_end
			FROM
				movie m
			LEFT JOIN ticket_price tp ON m.id = tp.movie_id)
			AS t1
		CROSS JOIN
			(SELECT
				tp.id,
				tp.event_start
			FROM movie m
			LEFT JOIN ticket_price tp ON m.id = tp.movie_id)
			AS t2
	WHERE t1.id <> t2.id AND t2.event_start >= t1.event_end -- не делал проверку наложений, можно добавить по аналогии с вариантом выше
	GROUP BY t1.id
	ORDER BY t1.event_start) AS all_time_between
WHERE all_time_between.длительность_перерыва >= '00:30:00';

-- "список фильмов, для каждого — с указанием общего числа посетителей за все время, среднего числа зрителей за сеанс
-- и общей суммы сборов по каждому фильму (отсортировать по убыванию прибыли). Внизу таблицы должна быть строчка
-- «итого», содержащая данные по всем фильмам сразу"...
SELECT * FROM
	(SELECT
		m.name AS фильм,
		SUM(film_by_event.visitor) AS всего_посетителей,
		AVG(film_by_event.visitor) AS среднее_число_посетителей_за_сеанс,
		SUM(total_price) AS общий_сбор
	FROM
		(SELECT
			t.movie_id AS movie_id,
			COUNT(*) AS visitor,
			SUM(t.price) AS total_price
		FROM purchase p
		LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
		GROUP BY t.movie_id, t.event_start) AS film_by_event
	LEFT JOIN movie m ON film_by_event.movie_id = m.id
	GROUP BY film_by_event.movie_id
	ORDER BY общий_сбор DESC) as a
UNION ALL -- пришлось обернуть внешний запросы еще в 1 select из-за UNION+ORDER_BY (делал ДЗ в MySQL). Можно было псевдо-поле сортировки ввести
SELECT * FROM
	(SELECT
		'Итого:',
    SUM(film_by_event.visitor) AS всего_посетителей,
		AVG(film_by_event.visitor) AS среднее_число_посетителей_за_сеанс,
		SUM(total_price) AS общий_сбор
	FROM
		(SELECT
			t.movie_id AS movie_id,
			COUNT(*) AS visitor,
			SUM(t.price) AS total_price
		FROM purchase p
		LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
		GROUP BY t.movie_id, t.event_start) AS film_by_event) as b;

-- "число посетителей и кассовые сборы, сгруппированные по времени начала фильма: с 9 до 15, с 15 до 18, с 18 до 21,
-- с 21 до 00:00 (сколько посетителей пришло с 9 до 15 часов и т.д.)"...
SELECT
	'с 9 до 15' AS интервал,
	m.name AS фильм,
  SUM(film_by_event.visitor) AS число_посетителей,
  SUM(film_by_event.total_price) AS кассовый_сбор
FROM
	(SELECT
	  TIME(t.event_start) AS event_start,
		t.movie_id AS movie_id,
		COUNT(*) AS visitor,
		SUM(t.price) AS total_price
	FROM purchase p
	LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
	GROUP BY t.movie_id, t.event_start) AS film_by_event
LEFT JOIN movie m ON film_by_event.movie_id = m.id
WHERE film_by_event.event_start >= '09:00:00' AND film_by_event.event_start < '15:00:00'
GROUP BY m.name
UNION ALL
SELECT
	'с 15 до 18' AS интервал,
	m.name AS фильм,
  SUM(film_by_event.visitor) AS число_посетителей,
  SUM(film_by_event.total_price) AS кассовый_сбор
FROM
	(SELECT
		TIME(t.event_start) AS event_start,
		t.movie_id AS movie_id,
		COUNT(*) AS visitor,
		SUM(t.price) AS total_price
	FROM purchase p
	LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
	GROUP BY t.movie_id, t.event_start) AS film_by_event
LEFT JOIN movie m ON film_by_event.movie_id = m.id
WHERE film_by_event.event_start >= '15:00:00' AND film_by_event.event_start < '18:00:00'
GROUP BY m.name
UNION ALL
SELECT
	'с 18 до 21' AS интервал,
	m.name AS фильм,
  SUM(film_by_event.visitor) AS число_посетителей,
  SUM(film_by_event.total_price) AS кассовый_сбор
FROM
	(SELECT
		TIME(t.event_start) AS event_start,
		t.movie_id AS movie_id,
		COUNT(*) AS visitor,
		SUM(t.price) AS total_price
	FROM purchase p
	LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
	GROUP BY t.movie_id, t.event_start) AS film_by_event
LEFT JOIN movie m ON film_by_event.movie_id = m.id
WHERE film_by_event.event_start >= '18:00:00' AND film_by_event.event_start < '21:00:00'
GROUP BY m.name
UNION ALL
SELECT
	'с 21 до 00' AS интервал,
	m.name AS фильм,
  SUM(film_by_event.visitor) AS число_посетителей,
  SUM(film_by_event.total_price) AS кассовый_сбор
FROM
	(SELECT
		TIME(t.event_start) AS event_start,
		t.movie_id AS movie_id,
		COUNT(*) AS visitor,
		SUM(t.price) AS total_price
	FROM purchase p
	LEFT JOIN ticket_price t ON p.ticket_price_id = t.id
	GROUP BY t.movie_id, t.event_start) AS film_by_event
LEFT JOIN movie m ON film_by_event.movie_id = m.id
WHERE film_by_event.event_start >= '21:00:00' AND film_by_event.event_start <= '23:59:59'
GROUP BY m.name;
